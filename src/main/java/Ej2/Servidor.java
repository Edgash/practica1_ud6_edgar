package Ej2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.*;

public class Servidor {

    public static void main(String[] args){
        //establecemos el puerto
        int PUERTO = 9090;
        ServerSocket sc;
        Socket so;
        DataOutputStream salida;
        DataInputStream entrada;
        String mensajeRecibido;

        try{
            //creamos el InetSocket con los datos del puerto
            sc = new ServerSocket(PUERTO);
            so = new Socket();
            while (true) {
                so = sc.accept();
                entrada = new DataInputStream(so.getInputStream());
                salida = new DataOutputStream(so.getOutputStream());
                //almacena el texto enviado desde el cliente
                mensajeRecibido = entrada.readUTF();
                //ponemos el texto enviado por el cliente en mayusculas
                String msgMayus = mensajeRecibido.toUpperCase();
                //si el texto no es 'quit'enviamos el mensaje en mayusculas pero si es 'quit' cerramos el servidor
                if (!msgMayus.equals("QUIT")) {
                    salida.writeUTF("" + msgMayus);
                } else {
                    sc.close();
                }
            }
        }catch(Exception ex){
            System.out.println("");
        }
    }
    }
