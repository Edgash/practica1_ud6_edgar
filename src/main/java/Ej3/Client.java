package Ej3;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class Client {
    //establecemos el puerto y la maquina que usaremos
    private static final int PORT_TO_LISTEN = 9090;
    private static final String FROM_MULTICAST_LISTEN = "230.0.0.1";

    public static void main(String[] args) {
        //creamos el InetSocket con los datos del puerto y la maquina
        InetSocketAddress inetSocketAddress = new InetSocketAddress(FROM_MULTICAST_LISTEN, PORT_TO_LISTEN);

        Scanner teclado = new Scanner(System.in);

        try (MulticastSocket s = new MulticastSocket(inetSocketAddress.getPort())) {
            s.joinGroup(inetSocketAddress.getAddress());
            //almacenamos el texto pedido por pantall en una variable
            System.out.println("Digale algo al servidor");
            String msg = teclado.nextLine();
            //establece la cantidad de bytes que mostrará el mensaje
            byte[] dato = new byte[1000];

            //enviamos el mensaje al servidor
            DatagramPacket hi = new DatagramPacket(msg.getBytes(), msg.length(),
                    inetSocketAddress.getAddress(), inetSocketAddress.getPort());
            s.send(hi);

            //recibimos el texto devuelto por el servidor y lo imprimimos
            DatagramPacket dgp = new DatagramPacket(dato, dato.length);
            s.receive(dgp);
            System.out.println(new String(dgp.getData(), 0, dgp.getLength()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
