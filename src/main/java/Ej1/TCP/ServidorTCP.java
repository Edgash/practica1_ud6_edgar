package Ej1.TCP;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;

public class ServidorTCP {
    //establecemos el puerto y la maquina que usaremos
    private static final int PORT = 9090;
    private static final String MACHINE = "localhost";

    public static void main(String[] args) {
        try{
            ServerSocket serverSocket = new ServerSocket();
            //creamos el InetSocket con los datos del puerto y la maquina
            InetSocketAddress sockAddr = new InetSocketAddress(MACHINE, PORT);
            serverSocket.bind(sockAddr);
            //creamos un hilo con el que pasará el mensaje del ClientMessage
            while(true) {
                ClientMessage cd = new ClientMessage(serverSocket.accept());
                Thread th = new Thread(cd);
                th.start();
            }
        }catch(IOException ex){
            System.out.println("\nCONNECTION ERROR: " + ex.getMessage());
        }
    }
}

