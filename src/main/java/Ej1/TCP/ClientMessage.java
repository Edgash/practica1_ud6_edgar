package Ej1.TCP;

import java.io.*;
import java.net.Socket;
import java.util.Date;

public class ClientMessage implements Runnable {

    Socket socket;
    //hacemos el constructor de la clase
    public ClientMessage(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            System.out.println("........ CLIENT CONNECTION ESTABLISHED ON PORT: " + socket.getPort());

            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            //establece el tiempo actual que el servidor le pasará al cliente
            Date fecha = new Date();
            String msg = "BIENVENIDO AL SERVIDOR SERENO, LA FECHA Y LA HORA ACTUALES SON: " + String.valueOf(fecha);
            bw.write(msg);
            bw.newLine();
            bw.flush();
            //cerramos tanto el BufferedReader, BufferedWriter y el Socket
            br.close();
            bw.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
