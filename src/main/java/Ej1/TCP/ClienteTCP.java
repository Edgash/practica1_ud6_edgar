package Ej1.TCP;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

public class ClienteTCP {
    //establecemos el puerto y la maquina que usaremos
    private static final int PORT = 9090;
    private static final String MACHINE = "localhost";

    public static void main(String[] args) {
        try {
            Socket clientSocket = new Socket();
            //creamos el InetSocket con los datos del puerto y la maquina
            InetSocketAddress sockAddr = new InetSocketAddress(MACHINE, PORT);
            clientSocket.connect(sockAddr);

            BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            bw.write(">>>> Hello I am a CLIENT <<<<");
            bw.newLine();
            bw.flush();
            //almacena el texto enviado del servidor y lo imprime por pantaalla
            String message = br.readLine();
            System.out.println(message);
            //cerramos tanto el BufferedReader, BufferedWriter y el Socket
            br.close();
            bw.close();
            clientSocket.close();
        } catch (IOException ex) {
            System.out.println("\nCONNECTION ERROR: " + ex.getMessage());
        }
    }
}
