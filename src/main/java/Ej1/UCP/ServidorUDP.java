package Ej1.UCP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.Date;

public class ServidorUDP {

        //establecemos el puerto y la maquina que usaremos
        private  static final int TO_PORT = 9090;
        private static final String TO_MULTICAST = "230.0.0.1";

        public static void main(String[] args) {
            while(true) {
            //establece el tiempo actual que se le pasará al cliente
            Date fecha = new Date();
            String msg = "BIENVENIDO AL SERVIDOR SERENO, LA FECHA Y LA HORA ACTUALES SON: " + String.valueOf(fecha);
            //creamos el InetSocket con los datos del puerto y la maquina
            InetSocketAddress inetSocketAddress = new InetSocketAddress(TO_MULTICAST, TO_PORT);

                try (DatagramSocket s = new DatagramSocket()) {
                    //creamos el datagramsocket que se enviará al cliente (almacenando el texto de 'msg')
                    DatagramPacket hi = new DatagramPacket(msg.getBytes(), msg.length(),
                            inetSocketAddress.getAddress(), inetSocketAddress.getPort());
                    //enviamos dicho mensaje
                    s.send(hi);

                } catch (IOException e) {
                    System.err.println(e.getLocalizedMessage());
                }
            }
        }
}
