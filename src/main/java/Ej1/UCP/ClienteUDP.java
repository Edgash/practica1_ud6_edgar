package Ej1.UCP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;

public class ClienteUDP {
        //establecemos el puerto y la maquina que usaremos
        private static final int PORT_TO_LISTEN = 9090;
        private static final String FROM_MULTICAST_LISTEN = "230.0.0.1";

        public static void main(String[] args) {
            //creamos el InetSocket con los datos del puerto y la maquina
            InetSocketAddress inetSocketAddress = new InetSocketAddress(FROM_MULTICAST_LISTEN, PORT_TO_LISTEN);

            try (MulticastSocket s = new MulticastSocket(inetSocketAddress.getPort())) {
                s.joinGroup(inetSocketAddress.getAddress());
                //establece la cantidad de bytes que mostrará el mensaje
                byte[] dato = new byte[1000];
                //con esto recibe el mensaje enviado por el servidor
                DatagramPacket dgp = new DatagramPacket(dato, dato.length);
                s.receive(dgp);
                //imprime el mensaje enviado por el servidor
                System.out.println(new String(dgp.getData(), 0, dgp.getLength()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

